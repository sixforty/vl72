#!/bin/sh
# $Id: minecraft.sh,v 1.1 2012/03/24 13:27:19 root Exp root $

# A wrapper script to start MineCraft.
# It should work with the Sun JRE/JDK as well as with the OpenJRE/JDK.

# If your computer has less than 2 GB of RAM you may have to set
# a lower value here (value is in MB):
MAXMEM=1024

# Fix for users of special IM modules
unset XMODIFIERS GTK_IM_MODULE QT_IM_MODULE

LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$JAVA_HOME/lib/amd64/:$JAVA_HOME/lib/i386/:$JAVA_HOME/jre/lib/amd64/:$JAVA_HOME/jre/lib/i386/" exec java -Xmx${MAXMEM}M -Xms$(($MAXMEM/2))M -jar /usr/share/minecraft/Minecraft.jar "$@"
~
