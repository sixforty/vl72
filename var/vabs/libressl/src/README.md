# LibreSSL

https://wiki.freebsd.org/LibreSSL

This is a fork of OpenSSL created by OpenBSD.

# WARNING:

This package conflicts with the openssl package in Vector|VLocity linux.
Installing this package will break a lot of other packages.  The most 
basic list of packages expected to break is below.  This list is by
no means complete, but if these packages are re-built after installing
LibreSSL, then other things can be updated as necessary.

- curl
- wget
- slapt-get

# Replacing openssl with LibreSSL

In order to prevent major damages to your OS, follow this procedure
to switch from openssl to LibreSSL.

- Clone the vabs tree for your Vector Linux version
- Download the source tarballs for the following packages
  - curl
  - wget
  - slapt-get
- Build the ``LibreSSL`` package.
- Remove the ``openssl`` package.
- Install the ``LibreSSL package.
- Build and upgradepkg the following packages in this order
  - curl
  - wget
  - slapt-get

This will restore your slapt-get and basic networking capabilities.

Other packages will begin to complain about missing libcrypto references and
segmentation faults.  These packages will need to be re-built against
LibreSSL as well.