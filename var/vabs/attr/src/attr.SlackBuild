#!/bin/sh

# Copyright 2005-2008, 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="attr"
VERSION=${VERSION:-2.4.47}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://download.savannah.gnu.org/releases/$NAME/$NAME-$VERSION.src.tar.gz"}

if [ "$NORUN" != 1 ]; then


# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp

PKG=$TMP/package-attr

if [ "$ARCH" = "x86_64" ]; then
  LIBDIRSUFFIX="64"
else
  LIBDIRSUFFIX=""
fi

rm -rf $PKG
mkdir -p $TMP $PKG
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
cd $TMP
rm -rf attr-$(echo $VERSION | cut -f 1 -d '-')
tar xvf $CWD/$NAME-$VERSION.src.tar.gz || exit 1
cd attr-$(echo $VERSION | cut -f 1 -d '-') || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# Modify the documentation directory so that it is a versioned directory: 
sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in

# Prevent installation of manual pages that were already installed by the man pages package: 
sed -i -e "/SUBDIRS/s|man2||" man/Makefile

# Add DESTDIR support and improve docs install location:
# zcat $CWD/attr.destdir.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1

# The 1/2 assed job has begun...  the onus of running autoconf should never
# be placed on packagers.  They may not have the same autotools versions.
autoconf

./configure \
  --prefix=/ \
  --exec-prefix=/ \
  --sbindir=/bin \
  --bindir=/usr/bin \
  --libdir=/lib${LIBDIRSUFFIX} \
  --libexecdir=/usr/lib${LIBDIRSUFFIX} \
  --includedir=/usr/include \
  --mandir=/usr/man \
  --datadir=/usr/share

make -j1 || exit 1 # DO NOT USE $NUMJOBS ON THIS PACKAGE
make install DESTDIR=$PKG
make install-dev DESTDIR=$PKG
make install-lib DESTDIR=$PKG

mv $PKG/usr/share/doc $PKG/usr
( cd $PKG/usr/doc ; mv attr attr-$VERSION )
#It would be nice to keep the same timestamps that the files have in the source:
rm -rf $PKG/usr/doc/attr-$VERSION/ea-conv
cp -a \
  README doc/COPYING* doc/PORTING doc/ea-conv \
  $PKG/usr/doc/attr-$VERSION

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
strip -g $PKG/usr/lib${LIBDIRSUFFIX}/*.a

# Remove bogus files:
rm -f $PKG/lib${LIBDIRSUFFIX}/*.a $PKG/lib${LIBDIRSUFFIX}/libattr.so $PKG/lib${LIBDIRSUFFIX}/*.la $PKG/usr/lib${LIBDIRSUFFIX}/*.la
# Make /usr/lib${LIBDIRSUFFIX}/libattr.so a symlink to /lib${LIBDIRSUFFIX}:
( cd $PKG/usr/lib${LIBDIRSUFFIX} ; rm -f libattr.so ; ln -sf /lib${LIBDIRSUFFIX}/libattr.so.1 libattr.so )
# Fix shared library perms:
chmod 755 $PKG/lib${LIBDIRSUFFIX}/*

# Gzip the man pages:
( cd $PKG/usr/man
  for i in `find . -type l` ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
  gzip -9 */*.?
)

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
