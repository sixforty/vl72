#!/bin/bash

## Tests for vlconfig2 package

check_vasm_reference() {
	# test if reference to 'vasm' in profile.d/vasm.sh has been changes
	# to 'vasm-legacy'
	printf "Testing 'vasm' reference in vasm.sh ..."
	grep "vasm-legacy" /etc/profile.d/vasm.sh
	if [ $? -gt 0 ]; then
		printf "FAILED"
		echo ""
		exit 1
	fi
	printf "PASS"
	echo ""
}
	
check_vasm_reference
