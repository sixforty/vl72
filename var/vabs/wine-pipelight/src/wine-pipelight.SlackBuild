#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .txz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
#
# This Template was compiled from the contributions of many users of the Vector
# Linux forum at http://forum.vectorlinux.com and from tidbits collected 
# from all over the internet. 
#
# Generated by sbbuilder-0.4.14.1, written by Rodrigo Bistolfi 
# (rbistolfi) and Raimon Grau Cusc� (Kidd) for VectorLinux.
#
# Please put your name below if you add some original scripting lines.
# AUTHORS = 

NAME="wine-pipelight"            #Enter package Name!
VERSION=${VERSION:-"1.7.19"}      #Enter package Version!
SRCNAM="wine"
PATNAM="wine-compholio"
COMPHOLIO=${COMPHOLIO:-"v${VERSION}-1"}
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"hata_ph"}   #Enter your Name!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"alsa-lib cyrus-sasl gcc-g++ gettext-tools glu \
gst-plugins-base gstreamer lcms2 libexif libgphoto2 mpg123 openal-soft \
openldap xz fontforge"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------

# If you do not want wine64 on Slackware64, set this to "no".
# Be sure to match this setting with the wine-pipelight build!
DO_WINE64=${DO_WINE64:-"no"}

# Where did we install wine-pipelight?
WPREFIX="/usr/libexec/$NAME"

# For matching wine_gecko & wine versions, see http://wiki.winehq.org/Gecko
GECKO=${GECKO:-2.24}

DOCS="ANNOUNCE AUTHORS COPYING.LIB ChangeLog LICENSE* README VERSION"

# Set the variable OPENGL to "NO" if you don't have a card that
# supports hardware accelerated OpenGL:
OPENGL=${OPENGL:-"YES"}    # Use 'YES' not 'yes' : case-sensitive!

# If you set REQUIRE_FONTFORGE to "NO" then the script won't refuse to build
#   wine-pipelight in case you don't have fontforge installed (it is needed to
#   generate the required base fonts).
REQUIRE_FONTFORGE=${REQUIRE_FONTFORGE:-"YES"}

#NUMJOBS=${NUMJOBS:" -j4 "}

if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
  SRC_ARCH="x86"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
  SRC_ARCH="x86_64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

#export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
#export CXXFLAGS=$CFLAGS
#export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS $SLKLDFLAGS"
#--------------------------------------------

#Enter URL for package here!
LINK=${LINK:-"http://downloads.sourceforge.net/project/${SRCNAM}/Source/${SRCNAM}-${VERSION}.tar.bz2 \
http://www.slackware.com/~alien/slackbuilds/wine-pipelight/build/wine-compholio-$COMPHOLIO.tar.gz \
http://downloads.sourceforge.net/wine/wine_gecko-${GECKO}-${SRC_ARCH}.msi"} 

#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------


rm -rf $PKG
cd $TMP
rm -rf $NAME-$VERSION


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xvf $CWD/$SRCNAM-$VERSION.tar.* || exit 1
#this moves whatever was extracted to the std dirname we are expecting
mv * $NAME-$VERSION &> /dev/null 2>&1
mkdir -p $PKG
#-----------------------------------------------------

# patch wine
tar xvf $CWD/$PATNAM-*.tar.* || exit 1
cd $PATNAM-*
make -C patches/ DESTDIR=$(cd ../${NAME}-${VERSION}; pwd) install || exit 1

cd $TMP/$NAME-$VERSION


#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
#for i in $CWD/patches/*;do
#  patch -p1 <$i
#  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
#  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
#done
#-----------------------------------------------------
 


#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

if [ "$ARCH" = "x86_64" -a "$DO_WINE64" = "yes" ]; then
  echo  "Also building wine64 (shared support, aka WoW64)"
fi

if [ "${OPENGL}" = "YES" -o "${OPENGL}" = "yes" ]; then
  do_opengl=""
else
  do_opengl="out"
fi

export CCAS="as"
export CFLAGS="$SLKCFLAGS"
export CXXFLAGS="$SLKCFLAGS"
export LDFLAGS="-L/usr/lib -ldl"
if [ "$ARCH" = "x86_64" ]; then
  export PKG_CONFIG_PATH="/usr/lib/pkgconfig:PKG_CONFIG_PATH"
fi

function wineconfigure ()
{
  ../configure \
    --prefix=${WPREFIX} \
    --localstatedir=${WPREFIX}/var \
    --sysconfdir=${WPREFIX}/etc \
    --mandir=${WPREFIX}/man \
    --with-x \
    --with${do_opengl}-opengl \
    --program-prefix= \
    --program-suffix= \
    --build=$CONFIGURE_TRIPLET \
    $*
}

autoreconf -vif

if [ "$ARCH" = "x86_64" -a "$DO_WINE64" = "yes" ]; then
  # First, wine64:
  mkdir wine64
  cd wine64
    wineconfigure --enable-win64 CC=/usr/bin/gcc || exit 1
    make depend || exit 1
    make || exit 1
    make DESTDIR=$PKG install || exit 1
  cd -
  # Next, wine:
  mkdir wine32
  cd wine32
    wineconfigure --with-wine64=../wine64 || exit 1
    make depend || exit 1
    make || exit 1
    make DESTDIR=$PKG install || exit 1
  cd -
else
  # No 64-bit wine requested, or we are on 32-bit Slackware:
  mkdir wine32
  cd wine32
    wineconfigure || exit 1
    make depend || exit 1
    make || exit 1
    make DESTDIR=$PKG install || exit 1
  cd -
fi


#######################################################################
#Miscellenious tweaks and things outside a normal ./configure go here #
#######################################################################

# Add the wine-gecko MSI installer(s) to the Wine package:
mkdir -p $PKG/${WPREFIX}/share/wine/gecko
install -m0644 $CWD/wine_gecko-${GECKO}-${SRC_ARCH}.msi $PKG/${WPREFIX}/share/wine/gecko/

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a ANNOUNCE AUTHORS COPYING.LIB ChangeLog LICENSE* README VERSION \
	$PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ];then
mkdir -p $PKG/usr/man
mv $PKG/usr/share/man/* $PKG/usr/man
rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;

if [ -d $PKG/usr/share/info ];then
mkdir -p $PKG/usr/info
mv $PKG/usr/share/info/* $PKG/usr/info
rm -rf $PKG/usr/share/info
fi 
find $PKG/usr/info -type f -exec gzip -9 {} \;

mkdir -p $PKG/install
if [ -d $PKG/usr/info ];then
cat >> $PKG/install/doinst.sh << EOF
CWD=\$(pwd)
cd usr/info
if [ -f dir ];then
    rm dir
fi
if [ -f dir.gz ];then
    rm dir.gz
fi
for i in *.info.gz;do
    install-info \$i dir
done
cd \$CWD
EOF
fi

# Create the doinst.sh script:
mkdir -p $PKG/install
cat <<EOT > $PKG/install/doinst.sh
# Remove copies of the libpipelight.so and re-create them.
# This is needed because of the way Mozilla browsers evaluate the available
# plugins in the plugins directory: multiple soft- and hardlinks
# to the same file will be seen as _one_ plugin, not several.
#
# Browser plugins need to be enabled by the user (non-root) using the command:
#   $ pipelight-plugin --enable <plugin_name>
# for instance if the user wants to use SilverLight:
#   $ pipelight-plugin --enable silverlight5.1
# That will download and install the necessary Windows files, the very first
# time the plugin is loaded by a browser.
#
chroot . pipelight-plugin --remove-mozilla-plugins
chroot . pipelight-plugin --create-mozilla-plugins
EOT

#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "$NAME")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 $NAME: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
$NAME: $NAME (Wine Is Not an Emulator)
$NAME:
$NAME: Wine is an Open Source implementation of the Windows API
$NAME: on top of X and Unix.
$NAME: This package contains a patched version of Wine, suitable
$NAME: for use with PipeLight, a cross-browser plugin.
$NAME: 
$NAME:
$NAME: License: GPL
$NAME: Authors: Refer to AUTHORS
$NAME: Website: http://fds-team.de/cms/pipelight-compile-wine.html

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\\$\ \.\/configure\ /" $TMP/$DIRNAME/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
fi
#--------------------------------------------------------------

# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
