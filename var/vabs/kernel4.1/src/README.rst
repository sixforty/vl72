Kernel Configuration

All changes to the kernel configuration should be placed on the
``config-common`` file.  This file gets appended to the 
architecture-specific base config file before compilation.

