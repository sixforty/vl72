Creating an OpenJDK (and OpenJRE) package for Slackware
=======================================================

In this directory you will find everything I used to create packages
for OpenJDK's JRE and JDK as an alternative to Oracle's binary-only releases
of Java.  Oracle no longer allows these binaries to be included with OS
distributions, so Slackware will no longer provide updates for them.
Build instructions for OpenJDK are provided further down in this README.

Or, you can just install my pre-compiled packages which I *also* provide!

If you want to install and just use my pre-built packages, then all you
really need are "rhino" which is the JavaScript engine, "icedtea-web"
which is the browser plugin, and one of "openjdk" or "openjre" packages,
depending of course on whether you need the full Java compiler suite or
only the Java Runtime Environment.

You will also need to install apache-ant if you want to compile OpenJDK
yourself.  In the past, xalan and xerces were needed separately as well
but that is no longer needed since apache-ant 1.8.1 and newer has a
fixed 'ant -diagnostics' command when xerces+xalan are not present.


Compiling OpenJDK from source
=============================

Compiling OpenJDK can be done in two ways.  The first way is called
"bootstrapping", which means compiling OpenJDK when there is no version of
the OpenJDK installed yet.  The SlackBuild uses the gcc java compiler for this
bootstrap (which means you have to have the "gcc-java" package installed).
The second way is to compile OpenJDK using an already installed version of
OpenJDK.  Such a pre-installed OpenJDK must have the same version (meaning,
OpenJDK 7 can only be compiled by another OpenJDK 7) and must have been
created with the help of "IcedTea" like my own openjdk,SlackBuild does.

The variable "BOOTSTRAP" inside the openjdk.SlackBuild determines which java
compiler will be used to compile OpenJDK.  If you want to use gcc's java, then
the command will be:

  # BOOTSTRAP=YES ./openjdk.SlackBuild

If you already have OpenJDK installed then you would start the compilation with:

  # BOOTSTRAP=NO ./openjdk.SlackBuild

By default, the variable in the script is set to "NO" which means that if
you run the openjdk.SlackBuild script without any parameters, it will use
a pre-esisting OpenJDK to compile the new package.

This SlackBuild script creates _two_ packages, the "openjdk" and the "openjre".
If you only need a Java Runtime then all you should install is the openjre
package - it is much smaller than the openjdk package.  The "openjdk" package
contains the Java compiler and SDK.


Java Web Plugin for OpenJDK
===========================

Suns's binary distribution of Java (taken over by Oracle) contains a Java web
plugin for mozilla-compatible browsers.  This plugin was not open sourced,
so OpenJDK needs a different solution in order to provide a web plugin for Java.
To this purpose, you have to (build and) install the "icedtea-web" package.
First compile and install OpenJDK (you need the openjdk package for this, not
the JRE), then compile icedtea-web.  The SlackBuild script plus sources are
in a separate directory.  Note that the icedtea-web plugin is not as mature
as the plugin which is shipped with Oracle's binaries.


Build-time requirements for OpenJDK and icedtea-web
===================================================

Slackware 14.0 and newer:
-------------------------
The gcc and seamonkey packages shipped with the distro can be used unmodified.

A word of caution.  All stable Slackware versions before "14.0" are
not able to compile OpenJDK "out of the box". You have to enhance two core
packages: "gcc" and "seamonkey" so that they become capable of compiling
OpenJDK.  Use the following instructions to fix-up gcc and seamonkey.
Either alternative is OK but running the shell scripts (the "b" options)
is a *lot* faster than recompiling these big packages.  These core Slackware
packages have be fixed in slackware-current.

Slackware 13.37:
----------------
You need to upgrade to the seamonkey* packages in the ./patches directory.
Choose one of these two alternatives to get a compatible gcc-java compiler:
 a) rebuild gcc with "--enable-java-home".  You can use the gcc.SlackBuild
    of Slackware-current which was modified accordingly.
 b) or run the script "create_gcj_jvm.sh" to achieve the same result you
    would get by rebuilding gcc: which is to create a directory structure
    of symlinks, which emulates the presence of a Sun JDK but for the
    GCC Java compiler.

Slackware 13.1 and older:
-------------------------
1) Choose one of these two alternatives for gcc:
   a) rebuild gcc with "--enable-java-home".  You can use the gcc.SlackBuild
      of Slackware-current which was modified accordingly.
   b) or run the script "create_gcj_jvm.sh" to achieve the same result you
      would get by rebuilding gcc: which is to create a directory structure
      of symlinks, which emulates the presence of a Sun JDK but for the
      GCC Java compiler.
2) Choose one of these two alternatives for seamonkey:
   a) rebuild seamonkey using the seamonkey.SlackBuild of Slackware-current
      which was modified accordingly.
   b) or run the script "fix_seamonkey_pkgconfig.sh" which does two things:
      add a pkgconfig file "libxul.pc" and modify an existing pkgconfig
      file, "seamonkey-plugin.pc"

Furthermore, you will need to have the following Apache Ant related packages
installed before starting the compilation of OpenJDK.
* apache-ant
* rhino

Please note that after installing apache-ant you will have to logout and
login again to initialize the ANT_HOME environment variable.

Only the "rhino" package has to remain on your system in order to *run* OpenJDK.
Apache-ant is only needed during the compilation and can optionally be removed
afterwards (but why should you).


==============================================================================
Eric Hameleers <alien at slackware.com>, 30-may-2014
