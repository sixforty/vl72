#!/bin/sh

# Copyright 2008, 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
NAME="SDL"
SRCNAME="SDL"
VERSION=${VERSION:-1.2.15}
IMAGE=${IMAGE:-1.2.12}
MIXER=${MIXER:-1.2.12}
NET=${NET:-1.2.8}
TTF=${TTF:-2.0.11}
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://www.libsdl.org/projects/SDL_image/release/SDL_image-$IMAGE.tar.gz"}
LINK1=${LINK1:-"http://www.libsdl.org/projects/SDL_mixer/release/SDL_mixer-$MIXER.tar.gz"}
LINK2=${LINK2:-"http://www.libsdl.org/projects/SDL_net/release/SDL_net-$NET.tar.gz"}
LINK3=${LINK3:-"http://www.libsdl.org/projects/SDL_ttf/release/SDL_ttf-$TTF.tar.gz"}
LINK4=${LINK4:-"http://www.libsdl.org/release/$SRCNAME-$VERSION.tar.gz"}
LINK5=${LINK5:-""}

MAKEDEPENDS="alsa-lib infozip libvorbis flac mesa glu libtiff libwebp smpeg fluidsynth !SDL"

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j7 "}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi
#get the source..........
for SRC in $(echo $LINK $LINK1 $LINK2 $LINK3 $LINK4);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-sdl

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf sdl-$VERSION
tar xf $CWD/SDL-$VERSION.tar.?z* || exit 1
cd SDL-$VERSION
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

#zcat $CWD/sdl.linux-2.6.31.input_absinfo.diff.gz | patch -p1 --verbose || exit 1

# We must use --disable-x11-shared or programs linked with SDL will
# crash on machines that use the closed source nVidia drivers.
zcat $CWD/patches/libsdl-1.2.15-resizing.patch.gz | patch -Np1 --verbose || exit 1
zcat $CWD/patches/sdl-1.2.14-fix-mouse-clicking.patch.gz | patch -Np1 --verbose || exit 1

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --disable-arts \
  --disable-esd \
  --enable-shared=yes \
  --enable-static=no \
  --disable-x11-shared || exit 1

make $NUMJOBS || make || exit 1

# Spam /, for mixer/image later on:
make install
# install to package:
make install DESTDIR=$PKG || exit 1
mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

mkdir -p $PKG/usr/doc/SDL-$VERSION/html
cp -a docs/index.html $PKG/usr/doc/SDL-$VERSION
cp -a docs/html/*.html $PKG/usr/doc/SDL-$VERSION/html
cp -a \
  BUGS COPYING CREDITS INSTALL README* TODO WhatsNew \
  $PKG/usr/doc/SDL-$VERSION

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

# Add SDL_image:
cd $TMP
rm -rf SDL_image-$IMAGE
tar xf $CWD/SDL_image-$IMAGE.tar.?z* || exit 1
cd SDL_image-$IMAGE
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# we don't want sdl to load the libs with dlopen(), gcc is smarter...
CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --enable-shared=yes \
  --enable-static=no \
  --enable-jpg-shared=no \
  --enable-png-shared=no \
  --enable-tif-shared=no

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1
mkdir -p $PKG/usr/doc/SDL_image-$IMAGE
cp -a \
  CHANGES COPYING README \
  $PKG/usr/doc/SDL_image-$IMAGE

# Add SDL_mixer:
cd $TMP
rm -rf SDL_mixer-$MIXER
tar xvf $CWD/SDL_mixer-$MIXER.tar.?z* || exit 1
cd SDL_mixer-$MIXER

# Don't look for things in /usr/local, since this is an installed package:
#zcat $CWD/SDL_mixer.usrlocal.diff.gz | patch -p1 --verbose || exit 1

# Fix default library path.  Don't use /usr/local, and use lib64 where needed:
sed -i "s,usr/local/lib,usr/lib${LIBDIRSUFFIX},g" timidity/config.h

# Install patched static libmikmod:
echo unzipping mikmod
unzip libmikmod-3.1.12.zip  || exit 1
( cd libmikmod-3.1.12.patched || exit 1
  patch -Np0 --verbose -i $CWD/patches/libmikmod-texi-doc-patch.diff || exit 1
  patch -Np0 --verbose -i $CWD/patches/libmikmod-texi-doc-patch2.diff || exit 1
  ./configure \
  --prefix=/usr/local \
  --libdir=/usr/local/lib${LIBDIRSUFFIX} \
  --with-pic \
  --enable-shared=no \
  --enable-static=yes
  make $NUMJOBS || make || exit 1
  make install || exit 1
) || exit 1

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --enable-music-mod \
  --enable-shared=yes \
  --enable-static=no || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

mkdir -p $PKG/usr/doc/SDL_mixer-$MIXER
cp -a \
  CHANGES COPYING README \
  $PKG/usr/doc/SDL_mixer-$MIXER

# We do not want to try to pull in -lmikmod, since that was linked static:
sed -i -e "s/ -lmikmod//g" $PKG/usr/lib${LIBDIRSUFFIX}/libSDL_mixer.la

# Add SDL_net:
cd $TMP
rm -rf SDL_net-$NET
tar xf $CWD/SDL_net-$NET.tar.?z* || exit 1
cd SDL_net-$NET
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --enable-shared=yes \
  --enable-static=no || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

mkdir -p $PKG/usr/doc/SDL_net-$NET
cp -a \
  CHANGES COPYING README \
  $PKG/usr/doc/SDL_net-$NET

# Add SDL_ttf:
cd $TMP
rm -rf SDL_ttf-$TTF
tar xf $CWD/SDL_ttf-$TTF.tar.?z* || exit 1
cd SDL_ttf-$TTF

#zcat $CWD/SDL_ttf-2.0.8-noftinternals.diff.gz | patch -p1 --verbose || exit 1

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --enable-shared=yes \
  --enable-static=no || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

mkdir -p $PKG/usr/doc/SDL_ttf-$TTF
cp -a \
  CHANGES COPYING README \
  $PKG/usr/doc/SDL_ttf-$TTF

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

gzip -9 $PKG/usr/man/man?/*.?

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
rm -rf $TMP

