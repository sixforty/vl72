#!/bin/bash

set -e

slapt-get -u
slapt-get -i python-enum34 python-cffi python-ipaddress python-pyasn1 python-idna python-six
TESTFILE=/tmp/pytest$$.py
echo ""
echo "Run simple cryptography test.."
echo ""
cat >>$TESTFILE <<FIN
#!/bin/env python
from cryptography.fernet import Fernet
import unittest

class TestCrypto(unittest.TestCase):
    def setUp(self):
        self.msg = b'Super secret message.  Keep it safe'
        key = Fernet.generate_key()
        self.f = Fernet(key)
    def test_encoding_decoding(self):
        token = self.f.encrypt(self.msg)
	output = self.f.decrypt(token)
	return self.assertEqual(self.msg, output)
if __name__ == '__main__':
    unittest.main()

FIN
python $TESTFILE
