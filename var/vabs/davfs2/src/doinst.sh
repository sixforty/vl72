# Add the group and user as necessary
getent group davfs2 || groupadd -g 230 davfs2 > /dev/null 2>&1
getent passwd davfs2 || useradd -u 230 -d /var/cache/davfs2 -g 230 -m davfs2 > /dev/null 2>&1
config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

config etc/davfs2/davfs2.conf.new > /dev/null 2>&1
config etc/davfs2/secrets.new > /dev/null 2>&1

