#!/bin/sh

# Copyright 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
NAME="pidentd"
VERSION=3.0.19
LINK=${LINK:-"http://slackware.osuosl.org/slackware-current/source/n/$NAME/$NAME-$VERSION.tar.gz"}
BUILDNUM=${BUILDNUM:-"3"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
MAKEDEPENDS="slapt-get openssl curl"
# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j4 "}
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-pidentd

rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf pidentd-$VERSION
tar xf $CWD/pidentd-$VERSION.tar.gz --checkpoint=10000 || exit 1
cd pidentd-$VERSION || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

zcat $CWD/pidentd.conf.diff.gz | patch -p1 --backup --verbose || exit

CFLAGS=-O2 \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --with-des-includes=/usr/include/openssl \
  --build=$ARCH-slackware-linux || exit 1

make $NUMJOBS || make || exit 1

mkdir -p $PKG/usr/sbin
cat src/identd > $PKG/usr/sbin/in.identd
cat src/ibench > $PKG/usr/sbin/ibench
cat src/idecrypt > $PKG/usr/sbin/idecrypt
cat src/ikeygen > $PKG/usr/sbin/ikeygen
chmod 755 $PKG/usr/sbin/*

mkdir -p $PKG/etc
cat etc/identd.conf > $PKG/etc/identd.conf.new

mkdir -p $PKG/usr/man/man8
cat doc/identd.8 | gzip -9c > $PKG/usr/man/man8/identd.8.gz
echo ".so man8/identd.8" | gzip -9c > $PKG/usr/man/man8/in.identd.8.gz

mkdir -p $PKG/usr/doc/pidentd-$VERSION
cp -a \
  BUGS ChangeLog FAQ INSTALL README Y2K doc/rfc* \
  $PKG/usr/doc/pidentd-$VERSION
chmod 644 $PKG/usr/doc/pidentd-$VERSION/*

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

mkdir -p $PKG/install
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

# Build the package:
cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP

